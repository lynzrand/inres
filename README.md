# inres

Inres is a testing platform for some Ingress-related stuff.

<!--

**Inres is NOT a Ingress third-party client, scraper or utility.**

Inres is in no way intended to violate the Terms of Service of Ingress. All data accessed by this project is only for educational and study use.

Inres comes with NO WARRANTY. The developer is not responsible for ANY data corruption, account punishment or any consequences of using the product of this project.

© 2018 Akaline Studio. All rights reserved until further notice.
 
-->