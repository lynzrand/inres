import 'package:flutter/material.dart';
import 'dart:math';

class InresDefaultStyles {
  // static Paint get defaultCirclePaintStyle {
  //   var i = Paint()
  //     ..color = Color(0xffaaaaaaa)
  //     ..blendMode = BlendMode.plus;
  //   return i;
  // }

  // static Paint get defaultLinePaintStyle {
  //   var i = Paint()
  //     ..strokeCap = StrokeCap.square
  //     ..strokeJoin = StrokeJoin.bevel
  //     ..color = Color(0xffaaaaaa)
  //     ..blendMode = BlendMode.plus;
  //   return i;
  // }

  static List<Offset> getPointPositions(Size size, [EdgeInsets padding]) {
    const sqrt3 = 1.73205;
    double minOfSize, xOffset, yOffset;
    if (padding != null) {
      size = padding.deflateSize(size);
      minOfSize = min(size.width, size.height);
      xOffset = (size.width - minOfSize) / 2 + padding.left;
      yOffset = (size.height - minOfSize) / 2 + padding.top;
    } else {
      minOfSize = min(size.width, size.height);
      xOffset = (size.width - minOfSize) / 2;
      yOffset = (size.height - minOfSize) / 2;
    }
    final r = minOfSize / 2;
    return <Offset>[
      Offset(r, 0),
      Offset(r * (1 - sqrt3 / 2), r / 2),
      Offset(r * (1 + sqrt3 / 2), r / 2),
      Offset(r * (1 - sqrt3 / 4), r * 3 / 4),
      Offset(r * (1 + sqrt3 / 4), r * 3 / 4),
      Offset(r, r),
      Offset(r * (1 - sqrt3 / 4), r * 5 / 4),
      Offset(r * (1 + sqrt3 / 4), r * 5 / 4),
      Offset(r * (1 - sqrt3 / 2), r * 3 / 2),
      Offset(r * (1 + sqrt3 / 2), r * 3 / 2),
      Offset(r, 2 * r),
    ].map((Offset o) => Offset(o.dx + xOffset, o.dy + yOffset)).toList();
  }
}
