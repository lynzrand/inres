import "package:flutter/material.dart";
import "dart:math";
import 'styles.dart';

final sqrt3 = sqrt(3);

class GlyphInterfacePainter extends CustomPainter {
  GlyphInterfacePainter({
    @required this.linkedVertexes,
    @required this.pointerPos,
    // this.circlePaintStyle,
    // this.linePaintStyle,
    @required this.circleRadius,
    this.padding,
    // this.activeCircleRadius,
    @required this.strokeWidth,
    this.debugRepaintEveryFrame = false,
    this.debugShowHitMargin,
    this.hitMargin,
  });
  List<int> linkedVertexes;
  Offset pointerPos;
  EdgeInsets padding;

  bool debugRepaintEveryFrame;
  bool debugShowHitMargin;
  double hitMargin;

  // Paint circlePaintStyle = InresDefaultStyles.defaultCirclePaintStyle;
  // Paint linePaintStyle = InresDefaultStyles.defaultLinePaintStyle;
  double circleRadius = 4.0;
  // double activeCircleRadius = 8.0;
  double strokeWidth = 2.0;

  void paint(Canvas canvas, Size size) {
    var linePaintStyle = Paint()
      ..strokeWidth = strokeWidth
      ..strokeCap = StrokeCap.square
      ..strokeJoin = StrokeJoin.bevel
      ..color = Color(0xff66ccff);
    // ..blendMode = BlendMode.plus;
    var circlePaintStyle = Paint()..color = Color(0xff66ccff);
    // ..blendMode = BlendMode.darken;
    var pointPositions = InresDefaultStyles.getPointPositions(size, padding);

    print("[Glypher] Painting on $canvas with $size and $padding.");

    if (debugShowHitMargin) {
      var hitMarginPaintStyle = Paint()..color = Color(0x22222222);
      pointPositions.forEach((Offset i) => canvas.drawCircle(i, hitMargin, hitMarginPaintStyle));
    }

    pointPositions.forEach((Offset i) => canvas.drawCircle(i, circleRadius, circlePaintStyle));

    if (linkedVertexes.isNotEmpty) {
      int last = linkedVertexes[0];
      for (int v in linkedVertexes) {
        canvas.drawLine(pointPositions[last], pointPositions[v], linePaintStyle);
        last = v;
      }
      if (pointerPos != null) canvas.drawLine(pointPositions[last], pointerPos, linePaintStyle);
    }
    // links.forEach((int i) => canvas.drawLine(
    //       pointPositions.elementAt(i.v1),
    //       pointPositions.elementAt(i.v2),
    //       linePaintStyle,
    //     ));
  }

  shouldRepaint(GlyphInterfacePainter oldOne) {
    return debugRepaintEveryFrame ||
        pointerPos != null ||
        oldOne.linkedVertexes != this.linkedVertexes ||
        oldOne.pointerPos != this.pointerPos ||
        oldOne.hitMargin != this.hitMargin ||
        oldOne.debugShowHitMargin != this.debugShowHitMargin;
  }
}

class GlyphCanvas extends StatefulWidget {
  @override
  _GlyphState createState() => _GlyphState();
}

class _GlyphState extends State<GlyphCanvas> {
  List<int> links = [];
  Offset pointerPos;
  Offset pointerStartPos;

  double hitMargin = 24.0;
  double firstPointHitMargin = 36.0;
  EdgeInsets padding = EdgeInsets.all(12.0);

  RenderBox get _renderBox {
    return context.findRenderObject() as RenderBox;
  }

  void hitCheck([double hitMargin]) {
    hitMargin ??= this.hitMargin;
    var points = InresDefaultStyles.getPointPositions(_renderBox.size);
    int hit = points.indexWhere((Offset i) => (i - pointerPos).distance < hitMargin);
    if (hit >= 0 && (links.isNotEmpty ? links[links.length - 1] != hitCheck : true)) {
      // print("[Glypher] Pointer started at point $hitCheck");
      setState(() {
        links.add(hit);
      });
    }
  }

  void _startDrawing(DragDownDetails d) {
    setState(() {
      links.clear();
      pointerPos = _renderBox.globalToLocal(d.globalPosition);
    });
    hitCheck(firstPointHitMargin);
  }

  void _continueDrawing(DragUpdateDetails d) {
    setState(() {
      pointerPos = _renderBox.globalToLocal(d.globalPosition);
    });
    hitCheck();
  }

  void _endDrawing([DragEndDetails d]) {
    setState(() {
      pointerPos = null;
    });
    // print("[Glypher] Pointer stopped dragging");
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: CustomPaint(
        painter: GlyphInterfacePainter(
          linkedVertexes: links,
          pointerPos: pointerPos,
          circleRadius: 4.0,
          strokeWidth: 2.0,
          debugShowHitMargin: false,
          hitMargin: hitMargin,
          padding: padding,
        ),
      ),
      onPanDown: _startDrawing,
      onPanUpdate: _continueDrawing,
      onPanEnd: _endDrawing,
      onPanCancel: _endDrawing,
    );
  }
}
